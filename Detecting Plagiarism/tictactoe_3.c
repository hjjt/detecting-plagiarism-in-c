
#include <stdio.h>
#include <stdlib.h>

char** createboard();
void print(char** Box);
int draw(char** Box);
char wincondition(char** Box, int i, int j);

int main() {
  char** Box = createboard();
  char winner = '\0';
  char r;
  char c;
  char round = 'O';

  while(!winner && !draw(Box)) {
    print(Box);


    printf("Player %c, make your move: ", round);
    fflush(stdout);
    scanf(" %c %c", &r, &c);


    int rowind = r - 'A';
    int colind = c - '1';
    if (Box[rowind][colind] == ' ') {
      Box[rowind][colind] = round;
      if (round == 'O') {
	       round = 'X';
      } else {
	       round = 'O';
      }
      winner = wincondition(Box, rowind, colind);
    } else {
      printf("Square occupied; try again.\n");
    }
  }

  print(Box);
  if (winner == 'X' || winner == 'O') {
    printf("Congratulations %c!\n", winner);
  } else {
    printf("Game ends in a draw.\n");
  }

  return 0;
}


char** createboard() {
  char** B = calloc(3, sizeof(char*));
  for(int i = 0; i < 3; ++i) {
    B[i] = calloc(3, sizeof(char));
  }

  for(int j=0; j < 3; ++j) {
    for(int k=0; k < 3; ++k) {
      B[j][k] = ' ';
    }
  }

  return B;
}

void print(char** Box) {
  printf(" |1|2|3|\n");
  for(int i = 0; i < 3; ++i) {
    printf("%c|", 'A' + i);
    for(int j = 0; j < 3; ++j) {
      printf("%c|", Box[i][j]);
    }
    printf("\n");
  }
}


int draw(char** Box) {
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      if (Box[i][j] == ' ') {

        return 0;
      }
    }
  }

  return 1;
}


char wincondition(char** Box, int i, int j) {
  if (Box[i][j] == Box[i][(j+1)%3] && Box[i][j] == Box[i][(j+2)%3])
  {
    return Box[i][j];
  }
  else if (Box[i][j] == Box[(i+1)%3][j] && Box[i][j] == Box[(i+2)%3][j])
  {
    return Box[i][j];
  }
  else if (i == j && Box[i][j] == Box[(i+1)%3][(j+1)%3] && Box[i][j] == Box[(i+2)%3][(j+2)%3])
  {
    return Box[i][j];
  }
  else if (i+j == 2 && Box[i][j] == Box[(i+2)%3][(j+1)%3] && Box[i][j] == Box[(i+1)%3][(j+2)%3])
  {
    return Box[i][j];
  }
  else {
    return 0;
  }
}
