#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

enum STATE {START,A,AU,AUT,AUTO,B,BR,BRE,BREA,BREAK,C,CA,CAS,CASE,CH,CHA,CHAR,CO,CON,CONS,CONST,CONT,CONTI,CONTIN,CONTINU,CONTINUE,D,DE,DEF,DEFA,DEFAU,DEFAUL,DEFAULT,DO,DOU,DOUB,DOUBL,DOUBLE,
  E,EL,ELS,ELSE,EN,ENU,ENUM,EX,EXT,EXTE,EXTER,EXTERN,F,FL,FLO,FLOA,FLOAT,FO,FOR,G,GO,GOT,GOTO,I,IF,IN,INT,L,LO,LON,LONG,R,RE,REG,REGI,REGIS,REGIST,REGISTE,REGISTER,RET,RETU,RETUR,RETURN,
  S,SH,SHO,SHOR,SHORT,SI,SIG,SIGN,SIGNE,SIGNED,SIZ,SIZE,SIZEO,SIZEOF,ST,STA,STAT,STATI,STATIC,STR,STRU,STRUC,STRUCT,SW,SWI,SWIT,SWITC,SWITCH,T,TY,TYP,TYPE,TYPED,TYPEDE,TYPEDEF,
  U,UN,UNI,UNIO,UNION,UNS,UNSI,UNSIG,UNSIGN,UNSIGNE,UNSIGNED,V,VO,VOI,VOID,VOL,VOLA,VOLAT,VOLATI,VOLATIL,VOLATILE,W,WH,WHI,WHIL,WHILE,PLUS,MINUS,EQUAL,MULTI,O_BRACK,C_BRACK};
enum STATE state=START;
int count=1;

enum STATE Start(char c){
  if (c=='a'){
    state=A;
  }
  else if (c=='b'){
    state=B;
  }
  else if (c=='c'){
    state=C;
  }
  else if (c=='d'){
    state=D;
  }
  else if (c=='e'){
    state=E;
  }
  else if (c=='f'){
    state=F;
  }
  else if (c=='g'){
    state=G;
  }
  else if (c=='i'){
    state=I;
  }
  else if (c=='l'){
    state=L;
  }
  else if (c=='r'){
    state=R;
  }
  else if (c=='s'){
    state=S;
  }
  else if (c=='t'){
    state=T;
  }
  else if (c=='u'){
    state=U;
  }
  else if (c=='v'){
    state=V;
  }
  else if (c=='w'){
    state=W;
  }
  else if (c=='r'){
    state=R;
  }
  else if (c=='+'){
    state=PLUS;
  }
  else if (c=='-'){
    state=MINUS;
  }
  else if (c=='='){
    state=EQUAL;
  }
  else if (c=='*'){
    state=MULTI;
  }
  else if (c=='('){
    state=O_BRACK;
  }
  else if (c==')'){
    state=C_BRACK;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE a(char c){
  if (c=='u'){
    state=AU;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Au(char c){
  if (c=='t'){
    state=AUT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Aut(char c){
  if (c=='o'){
    state=AUTO;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE b(char c){
  if (c=='r'){
    state=BR;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Br(char c){
  if (c=='e'){
    state=BRE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Bre(char c){
  if (c=='a'){
    state=BREA;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Brea(char c){
  if (c=='k'){
    state=BREAK;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE c(char c){
  if (c=='a'){
    state=CA;
  }
  else if (c=='h'){
    state=CH;
  }
  else if (c=='o'){
    state=CO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Ca(char c){
  if (c=='s'){
    state=CAS;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Cas(char c){
  if (c=='e'){
    state=CASE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Ch(char c){
  if (c=='a'){
    state=CHA;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Cha(char c){
  if (c=='r'){
    state=CHAR;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Co(char c){
  if (c=='n'){
    state=CON;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Con(char c){
  if (c=='s'){
    state=CONS;
  }
  else if (c=='t'){
    state=CONT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Cons(char c){
  if (c=='t'){
    state=CONST;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Cont(char c){
  if (c=='i'){
    state=CONTI;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Conti(char c){
  if (c=='n'){
    state=CONTIN;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Contin(char c){
  if (c=='u'){
    state=CONTINU;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Continu(char c){
  if (c=='e'){
    state=CONTINUE;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE d(char c){
  if (c=='e'){
    state=DE;
  }
  else if (c=='o'){
    state=DO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE De(char c){
  if (c=='f'){
    state=DEF;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Def(char c){
  if (c=='a'){
    state=DEFA;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Defa(char c){
  if (c=='u'){
    state=DEFAU;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Defau(char c){
  if (c=='l'){
    state=DEFAUL;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Defaul(char c){
  if (c=='t'){
    state=DEFAULT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Do(char c){
  if (c=='u'){
    state=DOU;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Dou(char c){
  if (c=='b'){
    state=DOUB;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Doub(char c){
  if (c=='l'){
    state=DOUBL;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Doubl(char c){
  if (c=='e'){
    state=DOUBLE;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE e(char c){
  if (c=='l'){
    state=EL;
  }
  else if(c=='n'){
    state=EN;
  }
  else if(c=='x'){
    state=EX;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE El(char c){
  if (c=='s'){
    state=ELS;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Els(char c){
  if (c=='e'){
    state=ELSE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE En(char c){
  if (c=='u'){
    state=ENU;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Enu(char c){
  if (c=='m'){
    state=ENUM;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Ex(char c){
  if (c=='t'){
    state=EXT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Ext(char c){
  if (c=='e'){
    state=EXTE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Exte(char c){
  if (c=='r'){
    state=EXTER;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Exter(char c){
  if (c=='n'){
    state=EXTERN;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE f(char c){
  if (c=='l'){
    state=FL;
  }
  else if (c=='o'){
    state=FO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Fl(char c){
  if (c=='o'){
    state=FLO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Flo(char c){
  if (c=='a'){
    state=FLOA;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Floa(char c){
  if (c=='t'){
    state=FLOAT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Fo(char c){
  if (c=='r'){
    state=FOR;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE g(char c){
  if (c=='o'){
    state=GO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Go(char c){
  if (c=='t'){
    state=GOT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Got(char c){
  if (c=='o'){
    state=GOTO;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE i(char c){
  if (c=='f'){
    state=IF;
  }
  else if (c=='n'){
    state=IN;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE In(char c){
  if (c=='t'){
    state=INT;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE l(char c){
  if (c=='o'){
    state=LO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Lo(char c){
  if (c=='n'){
    state=LON;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Lon(char c){
  if (c=='g'){
    state=LONG;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE r(char c){
  if (c=='e'){
    state=RE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Re(char c){
  if (c=='g'){
    state=REG;
  }
  else if (c=='t'){
    state=RET;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Reg(char c){
  if (c=='i'){
    state=REGI;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Regi(char c){
  if (c=='s'){
    state=REGIS;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Regis(char c){
  if (c=='t'){
    state=REGIST;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Regist(char c){
  if (c=='e'){
    state=REGISTE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Registe(char c){
  if (c=='r'){
    state=REGISTER;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Ret(char c){
  if (c=='u'){
    state=RETU;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Retu(char c){
  if (c=='r'){
    state=RETUR;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Retur(char c){
  if (c=='n'){
    state=RETURN;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE s(char c){
  if (c=='h'){
    state=SH;
  }
  else if (c=='i'){
    state=SI;
  }
  else if (c=='t'){
    state=ST;
  }
  else if (c=='w'){
    state=SW;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Sh(char c){
  if (c=='o'){
    state=SHO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Sho(char c){
  if (c=='r'){
    state=SHOR;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Shor(char c){
  if (c=='t'){
    state=SHORT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Si(char c){
  if (c=='g'){
    state=SIG;
  }
  else if (c=='z'){
    state=SIZ;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Sig(char c){
  if (c=='n'){
    state=SIGN;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Sign(char c){
  if (c=='e'){
    state=SIGNE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Signe(char c){
  if (c=='d'){
    state=SIGNED;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Siz(char c){
  if (c=='e'){
    state=SIZE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Size(char c){
  if (c=='o'){
    state=SIZEO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Sizeo(char c){
  if (c=='f'){
    state=SIZEOF;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE St(char c){
  if (c=='a'){
    state=STA;
  }
  else if (c=='r'){
    state=STR;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Sta(char c){
  if (c=='t'){
    state=STAT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Stat(char c){
  if (c=='i'){
    state=STATI;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Stati(char c){
  if (c=='c'){
    state=STATIC;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Str(char c){
  if (c=='u'){
    state=STRU;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Stru(char c){
  if (c=='c'){
    state=STRUC;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Struc(char c){
  if (c=='t'){
    state=STRUCT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Sw(char c){
  if (c=='i'){
    state=SWI;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Swi(char c){
  if (c=='t'){
    state=SWIT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Swit(char c){
  if (c=='c'){
    state=SWITC;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Switc(char c){
  if (c=='h'){
    state=SWITCH;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE t(char c){
  if (c=='y'){
    state=TY;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Ty(char c){
  if (c=='p'){
    state=TYP;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Typ(char c){
  if (c=='e'){
    state=TYPE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Type(char c){
  if (c=='d'){
    state=TYPED;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Typed(char c){
  if (c=='e'){
    state=TYPEDE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Typede(char c){
  if (c=='f'){
    state=TYPEDEF;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE u(char c){
  if (c=='n'){
    state=UN;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Un(char c){
  if (c=='i'){
    state=UNI;
  }
  else if (c=='s'){
    state=UNS;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Uni(char c){
  if (c=='o'){
    state=UNIO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Unio(char c){
  if (c=='n'){
    state=UNION;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Uns(char c){
  if (c=='i'){
    state=UNSI;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Unsi(char c){
  if (c=='g'){
    state=UNSIG;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Unsig(char c){
  if (c=='n'){
    state=UNSIGN;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Unsign(char c){
  if (c=='e'){
    state=UNSIGNE;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Unsigne(char c){
  if (c=='d'){
    state=UNSIGNED;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE v(char c){
  if (c=='o'){
    state=VO;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Vo(char c){
  if (c=='l'){
    state=VOL;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Vol(char c){
  if (c=='a'){
    state=VOLA;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Vola(char c){
  if (c=='t'){
    state=VOLAT;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Volat(char c){
  if (c=='i'){
    state=VOLATI;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Volati(char c){
  if (c=='l'){
    state=VOLATIL;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Volatil(char c){
  if (c=='e'){
    state=VOLATILE;
  }
  else{
    state=START;
  }
  return state;
}

enum STATE w(char c){
  if (c=='h'){
    state=WH;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Wh(char c){
  if (c=='i'){
    state=WHI;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Whi(char c){
  if (c=='l'){
    state=WHIL;
  }
  else{
    state=START;
  }
  return state;
}
enum STATE Whil(char c){
  if (c=='e'){
    state=WHILE;
  }
  else{
    state=START;
  }
  return state;
}

char* name = "dfa_file.txt";

void DFA(char* file1){
  FILE* fp=fopen(file1,"r+");
  FILE* fTmp = fopen(name, "w+");

  state=START;
  char letter = getc(fp);

  while (letter != EOF)
  {
    if (state==START){
      state=Start(letter);
    }
    else if(state==A){
      state=a(letter);
    }
    else if(state==AU){
      state=Au(letter);
    }
    else if(state==AUT){
      state=Aut(letter);
    }
    else if(state==AUTO){
      fputs("Auto", fTmp);
      state=START;
    }
    else if(state==B){
      state=b(letter);
    }
    else if(state==BR){
      state=Br(letter);
    }
    else if(state==BRE){
      state=Bre(letter);
    }
    else if(state==BREA){
      state=Brea(letter);
    }
    else if(state==BREAK){
      fputs("Break", fTmp);
      state=START;
    }
    else if(state==C){
      state=c(letter);
    }
    else if(state==CA){
      state=Ca(letter);
    }
    else if(state==CAS){
      state=Cas(letter);
    }
    else if(state==CASE){
      fputs("Case", fTmp);
      state=START;
    }
    else if(state==CH){
      state=Ch(letter);
    }
    else if(state==CHA){
      state=Cha(letter);
    }
    else if(state==CHAR){
      fputs("Char", fTmp);
      state=START;
    }
    else if(state==CO){
      state=Co(letter);
    }
    else if(state==CON){
      state=Con(letter);
    }
    else if(state==CONS){
      state=Cons(letter);
    }
    else if(state==CONST){
      fputs("Const", fTmp);
      state=START;
    }
    else if(state==CONT){
      state=Cont(letter);
    }
    else if(state==CONTI){
      state=Conti(letter);
    }
    else if(state==CONTIN){
      state=Contin(letter);
    }
    else if(state==CONTINU){
      state=Continu(letter);
    }
    else if(state==CONTINUE){
      fputs("Continue", fTmp);
      state=START;
    }

    else if(state==D){
      state=d(letter);
    }
    else if(state==DE){
      state=De(letter);
    }
    else if(state==DEF){
      state=Def(letter);
    }
    else if(state==DEFA){
      state=Defa(letter);
    }
    else if(state==DEFAU){
      state=Defau(letter);
    }
    else if(state==DEFAUL){
      state=Defaul(letter);
    }
    else if(state==DEFAULT){
      fputs("Default", fTmp);
      state=START;
    }
    else if(state==DO){
      state=Do(letter);
    }
    else if(state==DOU){
      state=Dou(letter);
    }
    else if(state==DOUB){
      state=Doub(letter);
    }
    else if(state==DOUBL){
      state=Doubl(letter);
    }
    else if(state==DOUBLE){
      fputs("Double", fTmp);
      state=START;
    }

    else if(state==E){
      state=e(letter);
    }
    else if(state==EL){
      state=El(letter);
    }
    else if(state==ELS){
      state=Els(letter);
    }
    else if(state==ELSE){
      fputs("Else", fTmp);
      state=START;
    }
    else if(state==EN){
      state=En(letter);
    }
    else if(state==ENU){
      state=Enu(letter);
    }
    else if(state==ENUM){
      fputs("Enum", fTmp);
      state=START;
    }
    else if(state==EX){
      state=Ex(letter);
    }
    else if(state==EXT){
      state=Ext(letter);
    }
    else if(state==EXTE){
      state=Exte(letter);
    }
    else if(state==EXTER){
      state=Exter(letter);
    }
    else if(state==EXTERN){
      fputs("Extern", fTmp);
      state=START;
    }
    else if(state==F){
      state=f(letter);
    }
    else if(state==FL){
      state=Fl(letter);
    }
    else if(state==FLO){
      state=Flo(letter);
    }
    else if(state==FLOA){
      state=Floa(letter);
    }
    else if(state==FLOAT){
      fputs("Float", fTmp);
      state=START;
    }
    else if(state==FO){
      state=Fo(letter);
    }
    else if(state==FOR){
      fputs("For", fTmp);
      state=START;
    }

    else if(state==G){
      state=g(letter);
    }
    else if(state==GO){
      state=Go(letter);
    }
    else if(state==GOT){
      state=Got(letter);
    }
    else if(state==GOTO){
      fputs("Goto", fTmp);
      state=START;
    }

    else if(state==I){
      state=i(letter);
    }
    else if(state==IF){
      fputs("If", fTmp);
      state=START;
    }
    else if(state==IN){
      state=In(letter);
    }
    else if(state==INT){
      fputs("Int", fTmp);
      state=START;
    }

    else if(state==L){
      state=l(letter);
    }
    else if(state==LO){
      state=Lo(letter);
    }
    else if(state==LON){
      state=Lon(letter);
    }
    else if(state==LONG){
      fputs("Long", fTmp);
      state=START;
    }

    else if(state==R){
      state=r(letter);
    }
    else if(state==RE){
      state=Re(letter);
    }
    else if(state==REG){
      state=Reg(letter);
    }
    else if(state==REGI){
      state=Regi(letter);
    }
    else if(state==REGIS){
      state=Regis(letter);
    }
    else if(state==REGIST){
      state=Regist(letter);
    }
    else if(state==REGISTE){
      state=Registe(letter);
    }
    else if(state==REGISTER){
      fputs("Register", fTmp);
      state=START;
    }
    else if(state==RET){
      state=Ret(letter);
    }
    else if(state==RETU){
      state=Retu(letter);
    }
    else if(state==RETUR){
      state=Retur(letter);
    }
    else if(state==RETURN){
      fputs("Return", fTmp);
      state=START;
    }

    else if(state==S){
      state=s(letter);
    }
    else if(state==SH){
      state=Sh(letter);
    }
    else if(state==SHO){
      state=Sho(letter);
    }
    else if(state==SHOR){
      state=Shor(letter);
    }
    else if(state==SHORT){
      fputs("Short", fTmp);
      state=START;
    }
    else if(state==SI){
      state=Si(letter);
    }
    else if(state==SIG){
      state=Sig(letter);
    }
    else if(state==SIGN){
      state=Sign(letter);
    }
    else if(state==SIGNE){
      state=Signe(letter);
    }
    else if(state==SIGNED){
      fputs("Signed", fTmp);
      state=START;
    }
    else if(state==SIZ){
      state=Siz(letter);
    }
    else if(state==SIZE){
      state=Size(letter);
    }
    else if(state==SIZEO){
      state=Sizeo(letter);
    }
    else if(state==SIZEOF){
      fputs("Sizeof", fTmp);
      state=START;
    }
    else if(state==ST){
      state=St(letter);
    }
    else if(state==STA){
      state=Sta(letter);
    }
    else if(state==STAT){
      state=Stat(letter);
    }
    else if(state==STATI){
      state=Stati(letter);
    }
    else if(state==STATIC){
      fputs("Static", fTmp);
      state=START;
    }
    else if(state==STR){
      state=Str(letter);
    }
    else if(state==STRU){
      state=Stru(letter);
    }
    else if(state==STRUC){
      state=Struc(letter);
    }
    else if(state==STRUCT){
      fputs("Struct", fTmp);
      state=START;
    }
    else if(state==SW){
      state=Sw(letter);
    }
    else if(state==SWI){
      state=Swi(letter);
    }
    else if(state==SWIT){
      state=Swit(letter);
    }
    else if(state==SWITC){
      state=Switc(letter);
    }
    else if(state==SWITCH){
      fputs("Switch", fTmp);
      state=START;
    }

    else if(state==T){
      state=t(letter);
    }
    else if(state==TY){
      state=Ty(letter);
    }
    else if(state==TYP){
      state=Typ(letter);
    }
    else if(state==TYPE){
      state=Type(letter);
    }
    else if(state==TYPED){
      state=Typed(letter);
    }
    else if(state==TYPEDE){
      state=Typede(letter);
    }
    else if(state==TYPEDEF){
      fputs("Typedef", fTmp);
      state=START;
    }

    else if(state==U){
      state=u(letter);
    }
    else if(state==UN){
      state=Un(letter);
    }
    else if(state==UNI){
      state=Uni(letter);
    }
    else if(state==UNIO){
      state=Unio(letter);
    }
    else if(state==UNION){
      fputs("Union", fTmp);
      state=START;
    }
    else if(state==UNS){
      state=Uns(letter);
    }
    else if(state==UNSI){
      state=Unsi(letter);
    }
    else if(state==UNSIG){
      state=Unsig(letter);
    }
    else if(state==UNSIGN){
      state=Unsign(letter);
    }
    else if(state==UNSIGNE){
      state=Unsigne(letter);
    }
    else if(state==UNSIGNED){
      fputs("Unsigned", fTmp);
      state=START;
    }

    else if(state==V){
      state=v(letter);
    }
    else if(state==VO){
      state=Vo(letter);
    }
    else if(state==VOL){
      state=Vol(letter);
    }
    else if(state==VOLA){
      state=Vola(letter);
    }
    else if(state==VOLAT){
      state=Volat(letter);
    }
    else if(state==VOLATI){
      state=Volati(letter);
    }
    else if(state==VOLATIL){
      state=Volatil(letter);
    }
    else if(state==VOLATILE){
      fputs("VOLATILE", fTmp);
      state=START;
    }

    else if(state==W){
      state=w(letter);
    }
    else if(state==WH){
      state=Wh(letter);
    }
    else if(state==WHI){
      state=Whi(letter);
    }
    else if(state==WHIL){
      state=Whil(letter);
    }
    else if(state==WHILE){
      fputs("While", fTmp);
      state=START;
    }

    else if(state==PLUS){
      fputs("+", fTmp);
      state=START;
    }
    else if(state==MINUS){
      fputs("-", fTmp);
      state=START;
    }
    else if(state==EQUAL){
      fputs("=", fTmp);
      state=START;
    }
    else if(state==MULTI){
      fputs("*", fTmp);
      state=START;
    }
    else if(state==O_BRACK){
      fputs("(", fTmp);
      state=START;
    }
    else if(state==C_BRACK){
      fputs("(", fTmp);
      state=START;
    }
    letter = getc(fp);
  }

  fclose(fp);
  fclose (fTmp);
  count=2;

  name="dfa_file2.txt";

}

// int main(int argc, char *argv[])
// {
//   clock_t t;
//   t = clock();
//
//   DFA(argv[1]);
//   DFA(argv[2]);
//
//   t = clock() - t;
//   double time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
//   printf("Time taken to execute(s): %fs \n", time_taken);
//
//  return 0;
// }
