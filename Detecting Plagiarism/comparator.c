#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
FILE *fp;
FILE *fp2;
FILE *fTmp;
FILE *fp3;
FILE *fTmp2;
void DFA(char*);
//default comparison of two files
void compare()
{

  char buffer = getc(fp);
  char buffer2 = getc(fp2);
  int line=1;
  int same=1;


  while (buffer != EOF || buffer2 != EOF)
  {
    // To count the line number
    if (buffer == '\n' && buffer2 == '\n')
    {
      line++;
    }

    // When two letters are different
    if (buffer != buffer2)
    {
      printf("\nFirst Difference is at character '%c' at line %i\n", buffer2, line);
      // No longer the same
      same=0;
    }

    // If its no longer the same
    if(same==0)
    {
      printf("\nThe two files are not identical\n");
      break;
    }

    buffer = getc(fp);
    buffer2 = getc(fp2);

  }

  // Check if it is still the same
  if(same==1)
  {
    printf("\nFiles are Identical\n");
  }

}

int max (int a, int b){
  if (a>=b){
    return a;
  }
  else{
    return b;
  }
}

int min(int a, int b, int c)
{
	if(a <= b && a <= c)
	{
		return a;
	}
	else if(b <= a && b <= c)
	{
		return b;
	}
	else if(c <= a && c <= b)
	{
		return c;
	}
}

int levenshtein(char* file1, char* file2) {

  fp=fopen(file1,"r+");
  fp2=fopen(file2,"r+");

  char * buffer = 0;
  char * buffer2 = 0;
  long length,length2;

  if (fp)
  {
    fseek (fp, 0, SEEK_END);
    length = ftell (fp);

    fseek (fp, 0, SEEK_SET);
    buffer = malloc (length);
    if (buffer)
    {
      fread (buffer, 1, length, fp);
    }
    fclose (fp);
  }

  if (fp2)
  {
    fseek (fp2, 0, SEEK_END);
    length2 = ftell (fp2);
    fseek (fp2, 0, SEEK_SET);
    buffer2 = malloc (length2);
    if (buffer2)
    {
      fread (buffer2, 1, length2, fp2);
    }
    fclose (fp2);
  }

  if (buffer && buffer2)
  {
    unsigned int s1len, s2len, x, y, lastdiag, olddiag;
    s1len = length;
    s2len = length2;
    unsigned int column[s1len + 1];
    for (y = 1; y <= s1len; y++)
        column[y] = y;
    for (x = 1; x <= s2len; x++) {
        column[0] = x;
        for (y = 1, lastdiag = x - 1; y <= s1len; y++) {
            olddiag = column[y];
            column[y] = min(column[y] + 1, column[y - 1] + 1, lastdiag + (buffer[y-1] == buffer2[x - 1] ? 0 : 1));
            lastdiag = olddiag;
        }
    }

    int maxlength=max(s1len,s2len);

    float distance =column[s1len];

    float percent = 100-(distance/maxlength)*100 ;
    // int percent = matrix[s2len][s1len];
    // printf("string is: %0.1f\n", distance);
    free(buffer);
    free(buffer2);



    return(percent);
  }

}

char* f_name = "keywords_file.txt";
void keywords(char* file1)
{
	char keyword[500] = {0};

  fp=fopen("keywords.txt","r+");
  fp2=fopen(file1,"r+");
  fTmp = fopen(f_name, "w+");
  char * buffer = 0;
  long length;

  if (fp2)
  {
    fseek (fp2, 0, SEEK_END);
    length = ftell (fp2);
    fseek (fp2, 0, SEEK_SET);
  }
  char word[length+1];

  rewind(fp2);
	while(1)
	{
    if(fscanf(fp2, "%s", word) != EOF)
    {
      while(fscanf(fp, "%s", keyword) != EOF)
      {
        if (strcmp(word,keyword)==0){
          fputs(word, fTmp);
        }
      }
      rewind(fp);
    }
    else{
      break;
    }
	}

  fclose (fp);
  fclose (fp2);
  fclose (fTmp);
  f_name= "keywords_file2.txt";
}

void prefixSuffixArray(char* pat, int M, int* pps) {
 int length = 0;
 pps[0] = 0;
 int i = 1;
 while (i < M) {
    if (pat[i] == pat[length]) {
       length++;
       pps[i] = length;
       i++;
    } else {
       if (length != 0)
       length = pps[length - 1];
       else {
          pps[i] = 0;
          i++;
       }
    }
 }
}
void KMPAlgorithm(char* file1, char* file2) {
  fp=fopen(file1,"r+");
  fp2=fopen(file2,"r+");

  char * buffer = 0;
  char * buffer2 = 0;
  long length,length2;

  if (fp)
  {
    fseek (fp, 0, SEEK_END);
    length = ftell (fp);
    fseek (fp, 0, SEEK_SET);
    buffer = malloc (length);
    if (buffer)
    {
      fread (buffer, 1, length, fp);
    }
    fclose (fp);
  }
  if (fp2)
  {
    fseek (fp2, 0, SEEK_END);
    length2 = ftell (fp2);
    fseek (fp2, 0, SEEK_SET);
    buffer2 = malloc (length2);
    if (buffer2)
    {
      fread (buffer2, 1, length2, fp2);
    }
    fclose (fp2);
  }

 // int M = strlen(pattern);
 // int N = strlen(text);
 int pps[length2];
 prefixSuffixArray(buffer2, length2, pps);
 int i = 0;
 int j = 0;
 int found=0;
 while (i < length) {
    if (buffer2[j] == buffer[i]) {
      j++;
      i++;
    }
    if (j == length2-1) {
      printf("Found pattern at index %d\n", i - j);
      j = pps[j - 1];
      found=1;
    }
    else if (i < length && buffer2[j] != buffer[i]) {
       if (j != 0){
         j = pps[j - 1];

       }
       else{
         i = i + 1;
       }
    }
 }
 if (found==0){
   printf("No pattern found\n");
 }
 free(buffer);
 free(buffer2);
}


int plagiarize(char* file1, char* file2){
  clock_t t;
  t = clock();

  keywords(file1);
  keywords(file2);
  float p = levenshtein("keywords_file.txt","keywords_file2.txt");
  printf("Keyword File: %0.1f%%\n",p);
  float q = levenshtein(file1,file2);
  printf("Original File: %0.1f%%\n",q);

  t = clock() - t;
  double time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
  printf("Time taken to execute(s): %fs \n", time_taken);
}

int main(int argc, char *argv[])
{

  int option;

  // When an option is given
  if (argc>=3)
  {
    // fp=fopen(argv[2],"r+");
    // fp2=fopen(argv[3],"r+");
    clock_t t;
    clock_t s;
    while((option = getopt(argc, argv, "dkelpqrsa")) != -1)
    {
      switch(option)
      {
        // Option to only use levenshtein algorithm without keywords extraction
        case 'l' :
          t = clock();

          float p = levenshtein(argv[2],argv[3]);
          printf("Plagirize Percentage: %0.1f%%\n",p);

          t = clock() - t;
          double time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
          printf("Time taken to execute(s): %fs \n", time_taken);
          break;

        // Option to use Keywords extraction on a single file only
        case 'e' :
          t = clock();

          keywords(argv[2]);

          t = clock() - t;
          time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
          printf("Time taken to execute(s): %fs \n", time_taken);
          break;

        // Option to find a specific pattern using KMP
        case 'k' :
          t = clock();

          keywords(argv[2]);
          KMPAlgorithm("keywords_file.txt","pattern.txt");

          t = clock() - t;
          time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
          printf("Time taken to execute(s): %fs \n", time_taken);
          break;

        // Option to use DFA instead of Keywords extration
        case 'd' :
          t = clock();

          DFA(argv[2]);
          DFA(argv[3]);

          p = levenshtein("dfa_file.txt","dfa_file2.txt");
          printf("Keyword File: %0.1f%%\n",p);
          float q = levenshtein(argv[2],argv[3]);
          printf("Original File: %0.1f%%\n",q);

          t = clock() - t;
          time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
          printf("Time taken to execute(s): %fs \n", time_taken);
          break;

        // //Option to perform levenshtein on orignal file
        // case 'p' :
        //   t = clock();
        //   q = levenshtein(argv[2],argv[3]);
        //   printf("Original File: %0.1f%%\n",q);
        //
        //   t = clock() - t;
        //   time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
        //   printf("Time taken to execute(s): %fs \n", time_taken);
        //   break;

        //Option to perform levenshtein on keywords extrated
        case 'q' :
          t = clock();
          keywords(argv[2]);
          keywords(argv[3]);
          p = levenshtein("keywords_file.txt","keywords_file2.txt");
          printf("Keyword File: %0.1f%%\n",p);

          t = clock() - t;
          time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
          printf("Time taken to execute(s): %fs \n", time_taken);
          break;

        //Option to perform levenshtein on keywords extrated and the original file
        case 'r' :
          plagiarize(argv[2],argv[3]);
          break;
        // Option to use DFA on a single file instead of Keywords extration
        case 's' :
          t = clock();

          DFA(argv[2]);

          t = clock() - t;
          time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
          printf("Time taken to execute(s): %fs \n", time_taken);
          break;

        //Option to perform keywords extraction and KMP pattern search
        case 'a':
          t = clock();
          for (int i=0;i<10;i++){
            s=clock();
            keywords(argv[2]);
            KMPAlgorithm("keywords_file.txt","pattern.txt");
            s = clock() - s;
            double time = ((double)s)/CLOCKS_PER_SEC; // calculate the elapsed time

            printf("Time taken to execute(s): %fs \n", time);
          }
          t = clock() - t;
          time_taken = (((double)t)/CLOCKS_PER_SEC)/10; // calculate the elapsed time

          printf("Average Time taken to execute(s): %fs \n", time_taken);
      }
    }
  }

  fclose(fp);
  fclose(fp2);

 return 0;
}
