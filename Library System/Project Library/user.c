#include <stdio.h>
#include <stdlib.h>
#include "Datastructure.h"

FILE *fp;
FILE *fTmp;

void addStudent(struct LIB info)
{
  // Gather information regarding new student
  printf ("Enter your name (no spaces allowed): ");
  scanf(" %s", info.sname);

  printf ("Enter your student ID:  ");
  scanf ("%d",&info.sid);

  printf ("New password: ");
  scanf ("%s",info.password);

  // Writes information into student file
  fp = fopen ("student.txt","a");
  fprintf(fp,"%s %d %s \n",info.sname, info.sid, info.password);
  fclose(fp);

  printf ("New record added\n");
}

void removeStudent (struct LIB info)
{
  int num;
  char buffer[1000];
  int count = 1;

  fp  = fopen("student.txt", "r");
  fTmp = fopen("tmp.txt", "w+");

  int i=1;
  // Scans for student information
  while(fscanf(fp,"%s %d %s\n",info.sname,&info.sid, info.password)!=EOF)
  {
    // Displays student information
    printf("%d) %s | %d | %s \n",i,info.sname,info.sid);
    i++;
  }

  // Reads student number to be removed
  printf("Please enter the student number you want to remove:");
  scanf("%d", &num);

  // To start from the start of the file
  rewind(fp);

  // checks if the number exists
  if (i>num)
  {
    while ((fgets(buffer, 1000, fp)) != NULL)
    {
      // Puts every line into the temporary file except the line with the student to be removed
      if (num != count)
      {
        fputs(buffer, fTmp);
      }
      // Increase the line number
      count++;
    }


    fclose(fp);
    fclose(fTmp);

    remove("student.txt");
    rename("tmp.txt", "student.txt");

    printf("\nStudent removed\n");
  }

  // If the number does not exist
  if (i<=num)
  {
    printf("Enter a proper number\n");
  }

}

void listStudents (struct LIB info )
{
  // Open the student file
  fp=fopen("student.txt","r+");

  // Check if it is empty
  if(fp != NULL)
  {
    int i=1;
    // Scan the student information
    while(fscanf(fp,"%s %d %s\n",info.sname,&info.sid, info.password)!=EOF)
    {
      // Displays student information
      printf("%d) %s | %d | %s \n",i,info.sname,info.sid);
      i++;
    }
  }

  // If the is not students in the file
  if (fp==NULL)
  {
    printf(" No Students recorded");
  }

  fclose(fp);
}

// Returns true if student record is found and false if it is not
int logIn (struct LIB info)
{
  int studentid;
  int found=0;
  char word[1000];
  int num;
  char word3[1000];
  char pass[25];

  // Request for student information
  printf ("\nEnter your student ID: ");
  scanf ("%d",&studentid);

  printf ("Enter your password: ");
  scanf ("%s",&pass);

  fp=fopen("student.txt","r+");

  // Scans the file
  while(fscanf(fp, "%s %d %s", word, &num, word3) != EOF)
  {
    // Check if information matches information in the file
    if(studentid==num)
    {
      if (strcmp(word3, pass)==0)
      {
        printf("\nStudent logged in\n");
        found=1;
        return 1;
      }
    }
  }

  // If it does not match
  if (found==0)
  {
    printf("Wrong ID or password\n");
    return 0;
  }

}


// Same concept as log in fucntion 
int libLog (struct LIB info)
{
  int libid;
  int found=0;
  char word[1000];
  int num;
  char word3[1000];
  char pass[25];

  printf ("\nEnter your ID: ");
  scanf ("%d",&libid);

  printf ("Enter your password: ");
  scanf ("%s",&pass);

  fp=fopen("librarian.txt","r+");

  while(fscanf(fp, "%s %d %s", word, &num, word3) != EOF)
  {

    if(libid==num)
    {
      if (strcmp(word3, pass)==0)
      {
        printf("\nLogged in\n");
        found=1;
        return 1;
      }
    }
  }

  if (found==0)
  {
    printf("Wrong ID or password\n");
    return 0;
  }

}
