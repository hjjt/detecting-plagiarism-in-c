project(library)

set( CMAKE_BUILD_TYPE Release)
#set( CMAKE_BUILD_TYPE Debug)

cmake_minimum_required(VERSION 2.8)


add_executable(lib
  books.c
  main.c
  user.c
)


set_property (TARGET lib PROPERTY C_STANDARD 99)
